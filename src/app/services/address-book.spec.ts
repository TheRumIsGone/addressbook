import { TestBed, inject } from '@angular/core/testing';
import { async, fakeAsync, tick } from '@angular/core/testing';

import { AddressBook } from './address-book';

describe('AddressBook', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				AddressBook
			]
		});
	});

	it('should initialize the service', inject([AddressBook], (service: AddressBook) => {
		expect(service).toBeTruthy();
	}));

});