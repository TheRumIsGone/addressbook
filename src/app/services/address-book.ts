import { Injectable } from '@angular/core';

export interface Employee {
	firstName: string,
	lastName: string,
	department: string,
	phoneNumber?: number
}

@Injectable()
export class AddressBook {
	state: Employee[] = [];

	addEmployee (employee: Employee) {
		this.state.push({...employee});
	}

	removeEmployee (index) {
		this.state.splice(index, 1)
	}
}
