import { TestBed, inject } from '@angular/core/testing';
import { async, fakeAsync, tick } from '@angular/core/testing';

import { AppStore } from './app-store';

describe('AppStore', () => {
	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				AppStore
			]
		});
	});

	it('should initialize the service', inject([AppStore], (service: AppStore) => {
		expect(service).toBeTruthy();
	}));

});