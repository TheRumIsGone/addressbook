import { Component } from '@angular/core';
import { AppStore, State } from './app-store';
import { AddressBook, Employee } from './services/address-book';

@Component({
	selector: 'app',
	templateUrl: 'app.template.html'
})
export class AppComponent {
	state: State;
	filterDepartment: string = '';
	filterNames: string = '';
	alphabetize: boolean = false;
	formState: Employee = {
		firstName: '',
		lastName: '',
		department: ''
	}

	buttonDisabled () {
		return !(this.formState.phoneNumber && this.formState.firstName && this.formState.lastName && this.formState.department);
	}

	constructor(public appStore: AppStore, private addressBook: AddressBook) {
		appStore.changes.subscribe((state: State) => {
			this.state = state;
		})
	}

	addEmployee = this.addressBook.addEmployee.bind(this.addressBook);
	removeEmployee = this.addressBook.removeEmployee.bind(this.addressBook);
}