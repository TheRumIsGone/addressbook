import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/distinctUntilChanged';
import { AddressBook, Employee } from './services/address-book';


export interface State {
  addressBook: Employee[]
}

const defaultState: State = {
  addressBook: []
}

const _store = new BehaviorSubject<State>(defaultState);

@Injectable()
export class AppStore {
  private _store = _store;
  changes = this._store
    .asObservable()
    .distinctUntilChanged()

  constructor(private addressBook: AddressBook) {
    this.setState({
      ...this._store.value,
      addressBook: addressBook.state
    });
  }

  setState(state: State) {
    this._store.next(state);
  }

  getState(): State {
    return this._store.value;
  }

  purge() {
    this._store.next(defaultState);
  }
}