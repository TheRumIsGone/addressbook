import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
	name: 'department'
})
export class DepartmentFilter implements PipeTransform {
	transform(items: any[], args: string): any {
		if (args) {
			return items.filter(item => item.department.indexOf(args) !== -1);
		} else {
			return items;
		}

	}
}

@Pipe({
	name: 'name'
})
export class nameFilter implements PipeTransform {
	transform(items: any[], args: string): any {
		if (args) {
			return items.filter(item => {
				const name = `${item.firstName} ${item.lastName}`;
				return name.indexOf(args) !== -1
			});
		} else {
			return items;
		}

	}
}