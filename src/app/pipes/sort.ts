import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
	name: 'alphabetize'
})
export class alphabetizeNames implements PipeTransform {
	transform(items: any[], args: boolean): any {
		if (args) {
			return [...items].sort((a, b) => {
				if (a.lastName < b.lastName) {
					return -1;
				} else {
					return 1;
				}
			});
		} else {
			return items;
		}
	}
}