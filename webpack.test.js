const webpack = require('webpack');
const { ContextReplacementPlugin } = webpack;

const root = require('./helpers/root');

module.exports = function (env) {
	const config = {
		devtool: 'inline-source-map',
		module: {
			rules: [
				{
					enforce: 'pre',
					test: /\.js$/,
					loader: 'source-map-loader',
					exclude: [
						// these packages have problems with their sourcemaps
						root('node_modules/rxjs'),
						root('node_modules/@angular')
					]
				},
				{
					test: /\.ts$/,
					use: [
						{
							loader: 'awesome-typescript-loader',
							query: {
								// use inline sourcemaps for "karma-remap-coverage" reporter
								sourceMap: false,
								inlineSourceMap: true
							},
						},
						{
							loader: 'angular2-template-loader'
						}
					]
				},
				{
					enforce: 'post',
					test: /\.(js|ts)$/,
					loader: 'istanbul-instrumenter-loader',
					include: root('src'),
					exclude: [
						/\.(e2e|spec)\.ts$/,
						/node_modules/
					]
				},
				/* Embed files. */
				{ 
					test: /\.(html|css)$/, 
					loader: 'raw-loader',
				}
			]
		},
		plugins: [
			new ContextReplacementPlugin(
				// The (\\|\/) piece accounts for path separators in *nix and Windows
				/angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
				root('src'), // location of your src
				{} // a map of your routes 
			),
		],
		resolve: {
			extensions: ['.ts', '.js'],
			modules: [root('src'), root('node_modules')]
		},
		// https://webpack.github.io/docs/configuration.html#node
		node: {
			global: true,
			process: false,
			crypto: 'empty',
			module: false,
			clearImmediate: false,
			setImmediate: false
		}
	}

	return config;
}